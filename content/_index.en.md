---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Art Course Sittard Geleen
description: Have you always wanted to really learn to paint and draw? Under expert supervision, in a nice atmosphere and on a high level Hennie Jansen teaches you both the basics and the intricacies of drawing and painting.
keywords: [Geleen, Sittard, art course, Hennie Jansen, paint, draw, painting school, drawing school]
sharing_image: school/biografie-hennie-jansen/zelfportret-hennie-jansen.jpg
---

## Learn to paint and draw on a high level

Have you always wanted to really learn to paint and draw? Under expert supervision, in a nice atmosphere and on a high level Hennie Jansen teaches you both the basics and the intricacies of drawing and painting. For more information about the school, the studio, and the teacher see the rest of the site, or contact us via [e-mail](#contact) or [phone](#contact).
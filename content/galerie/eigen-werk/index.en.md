---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Hennie's own work
url: /gallery/hennies-own-work
description: Some of Hennie's own artworks
sharing_image: galerie/eigen-werk/gallery/4.jpg
menu: 
    main:
        parent: Gallery
        weight: 1
gallery: 
    col1: 
        - 20.jpg
        - 2.jpg
        - 13.jpg
        - 15.jpg
        - 16.jpg
        - 4.jpg
        - 10.jpg
        - 24.jpg
    col2: 
        - 19.jpg
        - 3.jpg
        - 8.jpg
        - 22.jpg
        - 11.jpg
        - 14.jpg
        - 17.jpg
        - 18.jpg
        - 25.jpg
    col3:
        - 1.jpg
        - 6.jpg
        - 23.jpg
        - 7.jpg
        - 9.jpg
        - 5.jpg
        - 12.jpg
        - 26.jpg
---

## Some of Hennie's own artworks

Below are some of Hennie's own artworks. 

{{< gallery >}}
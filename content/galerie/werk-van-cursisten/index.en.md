---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Student's work
url: /gallery/students-work
description:  Drawing and paintings made by students of art course Sittard Geleen
sharing_image: galerie/werk-van-cursisten/gallery/tekeningen%20cursisten%2007.jpg
menu: 
    main:
        parent: Gallery
        weight: 2
gallery: 
    galerie:
        col1:
            - schilderijen cursisten 051.jpg
            - schilderijen cursisten 056.jpg
            - tekeningen cursisten 01.jpg
            - tekeningen cursisten 03.jpg
            - tekeningen cursisten 08.jpg        
            - schilderijen cursisten 001.jpg
            - schilderijen cursisten 002.jpg
            - schilderijen cursisten 003.jpg
            - schilderijen cursisten 004.jpg
            - schilderijen cursisten 005.jpg
            - schilderijen cursisten 006.jpg
            - schilderijen cursisten 007.jpg
            - schilderijen cursisten 008.jpg
            - schilderijen cursisten 009.jpg
            - schilderijen cursisten 010.jpg
            - schilderijen cursisten 011.jpg
            - schilderijen cursisten 012.jpg
            - schilderijen cursisten 013.jpg
            - schilderijen cursisten 014.jpg
            - schilderijen cursisten 015.jpg
            - schilderijen cursisten 016.jpg
            - truus_penders_geleen.jpg
            - jose kubben.jpg
            - Tonnie_Peters_Hoensbroek.jpg
            - cyrilla mols 2.jpg
            - bert heusschen 2.jpg
            - gezelligheid alom.jpg
            - HennyVeltstraOlieverfopdoek3-Spaubeek.jpg
            - EenpeizendeTonvanGeenen-buchten.jpg
            - anny mooren.jpg
            - AnjaGelissenAcrylopcanvas-OudStein.jpg
            - Myriam Raukema Olieverf op canvas-Geleen.jpg
            - wilma-van-linden-stein-olieverf-op-canvas.jpg
            - tonnie-peters-hoensbroek-olieverf-op-canvas.jpg
            - stefanie-vleeshouwers-limbricht-olieverf-op-canvas.jpg
            - ron-schouten-sittard-olieverf-op-canvas.jpg
        col2:
            - schilderijen cursisten 054.jpg
            - schilderijen cursisten 055.jpg
            - tekeningen cursisten 07.jpg
            - tekeningen cursisten 02.jpg
            - tekeningen cursisten 04.jpg
            - schilderijen cursisten 017.jpg
            - schilderijen cursisten 018.jpg
            - schilderijen cursisten 019.jpg
            - schilderijen cursisten 020.jpg
            - schilderijen cursisten 021.jpg
            - schilderijen cursisten 022.jpg
            - schilderijen cursisten 023.jpg
            - schilderijen cursisten 024.jpg
            - schilderijen cursisten 025.jpg
            - schilderijen cursisten 026.jpg
            - schilderijen cursisten 027.jpg
            - schilderijen cursisten 028.jpg
            - schilderijen cursisten 029.jpg
            - schilderijen cursisten 030.jpg
            - schilderijen cursisten 031.jpg
            - schilderijen cursisten 032.jpg
            - Imke_van_Lent_Tuddern.jpg
            - Marjo_Peters_Berg_aan_de_Maas.jpg
            - marjo peters 2.jpg
            - Marianne_Jacobs_Spaubeek.jpg
            - sybille gordebeke.jpg
            - marjo peters olieverf op canvas.jpg
            - tonnie peters.jpg
            - SybilleGordebekeOlieverfopcanvas-Buchten.jpg
            - CarinRittAcrylopdoek-Schimmert.jpg
            - JosSchwarzOlieverfopcanvas-Susteren.jpg
            - MarijBeckersOlieveropcanvas-Neerbeek.jpg
            - els-strijkers-geleen-olieverf-op-canvas.jpg
            - jeannij-simons-munstergeleen-olieverf-op-canvas.jpg
            - lies-vossen-sittard-olieverf-op-canvas.jpg
            - jos-schwarz-susteren-olieverf-op-canvas.jpg
            - carla-hamstra-geleen-olieverf-op-canvas.jpg
            - marij-beckers-beek-olieverf-op-canvas.jpg
        col3:
            - schilderijen cursisten 049.jpg
            - schilderijen cursisten 050.jpg
            - schilderijen cursisten 052.jpg
            - schilderijen cursisten 053.jpg
            - tekeningen cursisten 05.jpg
            - tekeningen cursisten 09.jpg
            - schilderijen cursisten 033.jpg
            - schilderijen cursisten 034.jpg
            - schilderijen cursisten 035.jpg
            - schilderijen cursisten 036.jpg
            - schilderijen cursisten 037.jpg
            - schilderijen cursisten 038.jpg
            - schilderijen cursisten 039.jpg
            - schilderijen cursisten 040.jpg
            - schilderijen cursisten 041.jpg
            - schilderijen cursisten 042.jpg
            - schilderijen cursisten 043.jpg
            - schilderijen cursisten 044.jpg
            - schilderijen cursisten 045.jpg
            - schilderijen cursisten 046.jpg
            - schilderijen cursisten 047.jpg
            - schilderijen cursisten 048.jpg
            - Sybille_Gordebeke_Buchten.jpg
            - Stefanie_Vleeshouwers_Limbricht.jpg
            - gertie lemmens 2.jpg
            - jos schwarz.jpg
            - Jos_Stevens.jpg
            - cyrilla mols.jpg
            - HennyVeltstraOlieverfopdoek2-Spaubeek.jpg
            - StefanieenMichelleVleeshouwers-Tekenenoppapier-Limricht.jpg
            - CarinRittAbstractAcrylopdoek-Schimmert.jpg
            - HennyVeltstraOlieverfopdoek-Spaubeek.jpg
            - margo-janssen-sittard-olieverf-op-canvas.jpg
            - petra-evertz-geleen-olieverf-op-canvas.jpg
            - miriam-raukema-geleen-olieverf-op-canvas.jpg
            - ingrid-riksen-sittard-olieverf-op-canvas.jpg
---

## Student's work

Below you can see some drawings and painting made by students. 

{{< gallery "galerie">}}

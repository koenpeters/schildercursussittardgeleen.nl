---
date: 2018-12-23T21:57:26+01:00
draft: false
title: The courses
url: /school/the-courses
description: "The courses of art Course Sittard Geleen"
sharing_image: school/de-cursussen/hennie-jansen-schilderend.jpg
menu: 
    main:
        parent: School
        weight: 1
---

## The courses
In her studio at Rijksweg Noord in Geleen, Hennie Jansen teaches her students how to paint and draw.

{{< img-resize hennie-jansen-schilderend Resize "330x" >}}float:left{{< /img-resize >}}

Under expert guidance, at a high level, and in a fun atmosphere you will learn both the basics and the intricacies of drawing and painting. You will learn and work at a pace that is comfortable to you and you choose the topics and materials that you are interested in. From oil paint, watercolor, acrylic and colored pencil, to pastels, charcoal, sand and gels. From (fine) painting, drawing, portrait and model to abstract.

Drawing and painting go together, so it is important that besides painting you also draw. During the first five lessons the most important basic skills such as perspective, sketching, tone values and observation will be taught. It is also possible to take a drawing course which deeper explores the basic techniques. Drawing can be alternated with painting. This is different for each student.

Each class contains both beginners and more  experienced students, which stimulates individual development and creativity.

The groups are small, which allows for individual guidance. This allows you to achieve a good result in a short period of time.

if you are interested in signing up for the course, or if you have questions, please contact us by filling out the [contact form](#contact) or by giving us a [call](#contact).

## Turpentine? Zest-it!

A student who works at the DSM chemical laboratories gave the advice to stop using Turpentine and start using Zest-it instead. Zest-it has the same characteristics as turpentine, but non of the negative side effects like the bad smell and the health consequences. Art school Hennie Jansen was one of the very first to make this switch and many schools have followed since. Working with Zest-it makes the experience of painting even more pleasant especially for people with respiratory problems. A real winner!
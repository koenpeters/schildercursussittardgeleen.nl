---
date: 2018-12-23T21:57:26+01:00
draft: false
title: De cursussen
description: De verschillende cursussen van teken- en schilderschool Hennie Jansen
sharing_image: school/de-cursussen/hennie-jansen-schilderend.jpg
menu: 
    main:
        parent: School
        weight: 1
---

## De cursussen
In haar gezellige atelier aan Rijksweg Noord in Geleen geeft Hennie Jansen schilder- en tekenlessen.

{{< img-resize hennie-jansen-schilderend Resize "330x" >}}float:left{{< /img-resize >}}

Onder deskundige begeleiding, op een hoog niveau, en in een leuke sfeer leert u zowel de basis als de fijne kneepjes van het tekenen en schilderen. U leert en werkt op een tempo dat u prettig vindt en u kiest zelf de onderwerpen en materialen waarin u geïnteresseerd bent. Van olieverf, aquarel, acryl en kleurpotlood, tot pastelkrijt, houtskool, zand en gels. Van (fijn)schilderen, tekenen, portret en model tot abstract.

Tekenen en schilderen gaan samen, daarom is het belangrijk dat er naast het schilderen ook getekend wordt. Tijdens de eerste vijf lessen worden de belangrijkste basisvaardigheden behandeld, zoals perspectief, schetsen, toonwaardes en het ontwikkelen van het observatievermogen. Ook is het mogelijk om een tekencursus te volgen die dieper ingaat op de basistechnieken. Het tekenen kan worden afgewisseld met schilderen. Dit is voor iedere cursist anders.  

Binnen iedere cursusgroep wordt gewerkt door zowel beginners als gevorderden hetgeen de ontwikkeling en de creativiteit sterk stimuleert.

De groepen zijn klein, hetgeen een persoonlijke begeleiding mogelijk maakt. Daardoor kunt u binnen korte tijd een behoorlijk resultaat behalen.

Bent u geïnteresseerd  of als u vragen hebt, neem dan contact op via het [contactformulier](#contact) of door te [bellen](#contact).

## Terpentijn? Zest-it!

Op aanraden van een cursist, die op het laboratorium werkt bij DSM, gebruik ik geen terpentijn, dat ongezond is voor de luchtwegen, maar Zest-it. Dit is niet schadelijk voor de gezondheid en heeft dezelfde werking als terpentijn, maar dan zonder de nare geur en gevolgen. Ik ben Zest-it een paar jaar geleden als eerste schilder- en tekenschool gaan gebruiken. Sindsdien wordt dit verkocht in steeds meer schilder- en hobbywinkels. Een echte aanrader!

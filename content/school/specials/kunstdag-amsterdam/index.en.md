---
date: 2012-07-01T10:00:00+01:00
draft: false
headless: true
title: "Art trip Amsterdam"
gallery: 
    col1: 
        - DSC05015.jpg
    col2: 
        - DSC05011.jpg
    col3: 
        - DSC05024.jpg
---

On this art trip we took the train to travel to Amsterdam and visit the Hermitage. The museum was showing an exhibition about the Impressionists: "Impressionism: Sensation & Inspiration". Because the van Gogh museum was being restored, many of his works were on display there as well. We finished the day with a nice meal in a restaurant in the the Amsterdam district of "De Jordaan". The restaurant happened to be the birth house of Dutch Singer and Comedian Wim Sonnevelt. A very inspiring and fun day!
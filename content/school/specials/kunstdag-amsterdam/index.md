---
date: 2012-07-01T10:00:00+01:00
draft: false
headless: true
title: "Kunstdag Amsterdam"
gallery: 
    col1: 
        - DSC05015.jpg
    col2: 
        - DSC05011.jpg
    col3: 
        - DSC05024.jpg
---

Wij hebben op deze kunstdag met de trein een bezoek gebracht aan Amsterdam. We zijn daar onder andere gaan kijken naar de Impressionisten in de Hermitage. Omdat het van Goghmuseum opgeknapt werd, waren ook de schilderijen van van Gogh te zien. Zeer de moeite waard. Daarna hebben we heerlijk gegeten in de Jordaan in het geboortehuis van Wim Sonnevelt, dat nu een restaurant geworden is.
---
date: 2018-04-01T10:00:00+01:00
draft: false
headless: true
title: "Etentje in Oud Geleen"
gallery: 
    col1: 
        - groep.jpeg
        - cursist-en-docent.jpg
        - bord-met-eten.jpeg
    col2:
        - eten.jpeg
        - gezellige-gesprekken.jpg
        - meer-gezellige-gesprekken.jpg
        - cursisten-proosten.jpg
    col3:
        - grappige-ober.jpg
        - lekker-nagerecht.jpg
        - vuurwerk.jpg
---

Regelmatig stellen de cursisten voor om weer eens een gezellig avondje uit te gaan. In dit geval een heerlijk etentje bij Het Kleine Verschil in Oud Geleen. Het was erg gezellig en het werd weer laat. Het sneeuwde buiten en wij zaten gezellig binnen.

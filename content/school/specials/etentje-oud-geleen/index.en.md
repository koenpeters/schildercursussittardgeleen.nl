---
date: 2018-04-01T10:00:00+01:00
draft: false
headless: true
title: "Dinner in Oud Geleen"
gallery: 
    col1: 
        - groep.jpeg
        - cursist-en-docent.jpg
        - bord-met-eten.jpeg
    col2:
        - eten.jpeg
        - gezellige-gesprekken.jpg
        - meer-gezellige-gesprekken.jpg
        - cursisten-proosten.jpg
    col3:
        - grappige-ober.jpg
        - lekker-nagerecht.jpg
        - vuurwerk.jpg   
---

Frequently the students propose to go out for a nice evening with the whole group. In this case we had a very nice dinner in Het Kleine Verschil in Oud Geleen. It was a great evening which was made extra special because of the snowfall that surprised us all.
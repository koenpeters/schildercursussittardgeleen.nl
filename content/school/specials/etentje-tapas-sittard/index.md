---
date: 2022-01-29T10:00:00+01:00
draft: false
headless: true
title: "Tapas in Sittard"
gallery: 
    col1: 
        - tapas-1.jpg
    col2:
        - tapas-2.jpg
---

Het was weer hoog tijd om een gezellig avondje uit te gaan. In dit geval heerlijk tapas gegeten in Sittard met een leuke groep cursisten. 

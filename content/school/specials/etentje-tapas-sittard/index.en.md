---
date: 2022-01-29T10:00:00+01:00
draft: false
headless: true
title: "Tapas in Sittard"
gallery: 
    col1: 
        - tapas-1.jpg
    col2:
        - tapas-2.jpg
---

My students proposed to go out for a nice evening with the whole group. In this case we had Tapas in Sittard. Once again we had a great evening.
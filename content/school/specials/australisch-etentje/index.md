---
date: 2013-10-01T10:00:00+01:00
draft: false
headless: true
title: "Australisch etentje"
gallery: 
    col1: 
        - etentje_1.jpg
---

Het was weer erg leuk om bij te kletsen buiten het atelier. Met gezellige cursisten van mijn schilder- en tekenschool, Els, Truus, Myriam, Ingrid en Imke, uit eten in een Australisch restaurant in Sittard. Dus... Kangoeroe proeven natuurlijk. En het smaakte heerlijk!
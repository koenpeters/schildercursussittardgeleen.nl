---
date: 2013-10-01T10:00:00+01:00
draft: false
headless: true
title: Australian Dinner
gallery: 
    col1: 
        - etentje_1.jpg
---

A very enjoyable evening with some of my students: Els, Truus, Myriam, Ingrid en Imke. We went to an Australian restaurant in Sittard. It is always very nice to find some time to talk outside of the classroom, especially if the conversations are accompanied with a "local" dish like kangaroo.
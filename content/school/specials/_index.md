---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Specials
description: Kunstuitstapjes, speciale cursussen, tentoonstellingen en andere speciale gebeurtenissen bij teken- en schilderschool Sittard Geleen
sharing_image: school/specials/etentje-oud-geleen/gallery/groep.jpeg
layout: specials
menu: 
    main:
        parent: School
        weight: 3
---

## Specials

Regelmatig geef ik, altijd met groot succes, tentoonstellingen van de mooie werken van mijn cursisten, o.a. in de [Terpkerk in Urmond](http://www.urmondmonumenten.nl/terpkerk.html). Ook is het altijd erg gezellig als we een of twee keer per jaar erop uit gaan en een kunstdag houden. Op zulke dagen bezoeken we musea en gaan we aan het einde van de dag gezellig samen eten. De Hermitage in Amsterdam stond op het programma, waarbij we de prachtige tentoonstelling: Impressionisme, sensatie en inspiratie bezochten. Ook worden er workshops georganiseerd om speciale technieken te leren zoals naaktmodel tekenen. Op deze pagina vindt u foto's en korte verslagen van dit soort speciale dagen en informatie over andere relevante onderwerpen.
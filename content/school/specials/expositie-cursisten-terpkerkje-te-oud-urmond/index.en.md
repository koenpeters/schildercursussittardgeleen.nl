---
date: 2011-08-01T10:00:00+01:00
draft: false
headless: true
title: "Exhibition student's work at Terpkerkje in Old Urmond"
gallery: 
    col1: 
        - IMG_1081.jpg
        - DSC01591.jpg
    col2: 
        - DSC01617.jpg
        - DSC01582.jpg
        - DSC01586.jpg
    col3: 
        - DSC01608.jpg
        - DSC01614.jpg
        - DSC01575.jpg
---

The art school has organized multiple exhibitions of some of the best works of the students. These exhibitions took place at the nicely situated Terpkerkje in Old Urmond. During these days friends, family and other interested people were able to take a look at the art and join the lessons that were being held in the old church's exhibition room.
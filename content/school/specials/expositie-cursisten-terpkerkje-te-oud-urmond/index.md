---
date: 2011-08-01T10:00:00+01:00
draft: false
headless: true
title: "Expositie cursisten Terpkerkje te Oud Urmond"
gallery: 
    col1: 
        - IMG_1081.jpg
        - DSC01591.jpg
    col2: 
        - DSC01617.jpg
        - DSC01582.jpg
        - DSC01586.jpg
    col3: 
        - DSC01608.jpg
        - DSC01614.jpg
        - DSC01575.jpg
---

De school heeft meerdere exposities georganiseerd waarbij de werken van de cursisten in het middelpunt stonden. Deze exposities vonden plaats in het mooie en sfeervolle terpkerkje in Oud Urmond. Gedurende deze dagen konden vrienden, familie en andere geïnteresseerden de werken zien en werden er lessen gegeven in het kerkje zelf.
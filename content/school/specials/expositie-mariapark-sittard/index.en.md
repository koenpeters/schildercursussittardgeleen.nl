---
date: 2016-08-28T10:00:00+01:00
draft: false
headless: true
title: "Exposition Mariapark Sittard"
gallery: 
    col1: 
        - expo-2016-krantenknipsel.jpg
        - expo-2016-hennie-jansen.jpg
        - expo-2016-locatie-buiten2.jpg
        - expo-2016-locatie-buiten1.jpg
        - expo-2016-overzicht10.jpg
    col2: 
        - expo-2016-overzicht02.jpg
        - expo-2016-overzicht01.jpg
        - expo-2016-locatie-interieur3.jpg
        - expo-2016-overzicht03.jpg
        - expo-2016-overzicht06.jpg
        - expo-2016-overzicht05.jpg
    col3: 
        - expo-2016-overzicht08.jpg
        - expo-2016-overzicht07.jpg
        - expo-2016-locatie-interieur2.jpg
        - expo-2016-locatie-interieur1.jpg
        - expo-2016-overzicht04.jpg
---

In August the school organised an exposition in the Mariapark in Sittard. Work of the teacher and of her students was on display for a full week in the lovely neogothic building in the heart of the town right across the Onze-Lieve-Vrouw van het Heilig Hart basilica. The light that shined through the high stained glass windows gave extra allure to the paintings and drawings that were on display. 
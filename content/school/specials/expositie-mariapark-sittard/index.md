---
date: 2016-08-28T10:00:00+01:00
draft: false
headless: true
title: "Expositie Mariapark Sittard"
gallery: 
    col1: 
        - expo-2016-krantenknipsel.jpg
        - expo-2016-hennie-jansen.jpg
        - expo-2016-locatie-buiten2.jpg
        - expo-2016-locatie-buiten1.jpg
        - expo-2016-overzicht10.jpg
    col2: 
        - expo-2016-overzicht02.jpg
        - expo-2016-overzicht01.jpg
        - expo-2016-locatie-interieur3.jpg
        - expo-2016-overzicht03.jpg
        - expo-2016-overzicht06.jpg
        - expo-2016-overzicht05.jpg
    col3: 
        - expo-2016-overzicht08.jpg
        - expo-2016-overzicht07.jpg
        - expo-2016-locatie-interieur2.jpg
        - expo-2016-locatie-interieur1.jpg
        - expo-2016-overzicht04.jpg
---

In augustus heeft de schilderschool een expositie georganiseerd in het Mariapark in Sittard. Werk van de lerares en haar studenten werd een volle week geëxposeerd in het zeer fraaie neogotische gebouw dat ligt in het hart van Sittard, tegenover de Basiliek van Onze-Lieve-Vrouw van het Heilig Hart. Het licht dat door de hoge glas in lood ramen van rijksmonument scheen gaf de werken extra allure.
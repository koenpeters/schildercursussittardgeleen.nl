---
date: 2015-10-01T10:00:00+01:00
draft: false
headless: true
title: "Een nieuwe start van het seizoen"
gallery: 
    col1: 
        - taart-eten-en-schilderen-is-goed-voor-het-humeur.jpg
        - taart-tijdens-de-les.jpg
    col2: 
        - taart-eten-tijdens-het-schilderen.jpg
        - taart-tractatie-tijdens-het-schilderen.jpg
    col3: 
        - taart-heerlijk-gebak-tussen-de-penseelstreken-door.jpg
---

Het nieuwe seizoen begint weer in september, ik verheug me er alweer op! Waarop? Nou: vla eten, schilderen, van het uitzicht genieten vanuit het atelier, gezelligheid, geconcentreerd bezig zijn waardoor de prachtigste werken tevoorschijn getoverd worden door de cursisten. Een goede basis ontwikkelen waaruit professionaliteit voortvloeit. Dit moet eigenlijk het eerste genoemd worden in het rijtje. Maar dat spreekt vanzelf, natuurlijk!
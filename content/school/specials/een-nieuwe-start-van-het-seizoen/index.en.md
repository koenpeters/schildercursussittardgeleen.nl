---
date: 2015-10-01T10:00:00+01:00
draft: false
headless: true
title: A fresh start of the new season
gallery: 
    col1: 
        - taart-eten-en-schilderen-is-goed-voor-het-humeur.jpg
        - taart-tijdens-de-les.jpg
    col2: 
        - taart-eten-tijdens-het-schilderen.jpg
        - taart-tractatie-tijdens-het-schilderen.jpg
    col3: 
        - taart-heerlijk-gebak-tussen-de-penseelstreken-door.jpg
---

Every now and then the lessons are extra inspiring because of the cake or other treats. This time the occasion was the end of the school year which is a nice time to reflect on the lessons learned and all artistic things that lie ahead in the coming year. The final brush strokes were extra well placed partly because of the lovely taste of the apricot cake with whipped cream.
---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Specials
url: /school/specials_en
description: Art excursions, special courses, exhibitions and other special activities at art course Sittard Geleen
sharing_image: school/specials/etentje-oud-geleen/gallery/groep.jpeg
layout: specials
menu: 
    main:
        parent: School
        weight: 3
---

## Specials

Regularly I arrange exhibitions displaying some of mine and my student's best work. We try to pick exhibition locations that add to the experience such as [the terpkerk in Urmond](http://www.urmondmonumenten.nl/terpkerk.html). Twice a year we also go on "art trips" to museums throughout the country. We always end these trips with a great meal in a nice restaurant going over the beautiful and inspiring works that we saw that day. For example: we visited the exposition "Impressionism: Sensation & Inspiration" in the Hermitage in Amsterdam. I also organise special drawing events such as nude figure drawing which is always a very enjoyable experience for all that attend. On this page you can read short impressions of these events and browse through some nice pictures that we took.
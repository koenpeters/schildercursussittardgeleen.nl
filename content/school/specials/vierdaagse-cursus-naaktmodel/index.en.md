---
date: 2013-07-01T10:00:00+01:00
draft: false
headless: true
title: "Four day course in drawing nudes"
gallery: 
    col1: 
        - naaktmodel_tekenen_01.jpg
        - naaktmodel_tekenen_04.jpg
        - naaktmodel_tekenen_07.jpg
        - naaktmodel_tekenen_10.jpg
        - naaktmodel_tekenen_14.jpg
    col2: 
        - naaktmodel_tekenen_03.jpg
        - naaktmodel_tekenen_06.jpg
        - naaktmodel_tekenen_09.jpg
        - naaktmodel_tekenen_12.jpg
    col3: 
        - naaktmodel_tekenen_02.jpg
        - naaktmodel_tekenen_05.jpg
        - naaktmodel_tekenen_11.jpg
        - naaktmodel_tekenen_15.jpg
---

Each summer the school offers a four day figure drawing course, in which the students learn the basic techniques that are necessary to draw a nude figure using a professional model.
---
date: 2013-07-01T10:00:00+01:00
draft: false
headless: true
title: "Vierdaagse cursus naaktmodel"
gallery: 
    col1: 
        - naaktmodel_tekenen_01.jpg
        - naaktmodel_tekenen_04.jpg
        - naaktmodel_tekenen_07.jpg
        - naaktmodel_tekenen_10.jpg
        - naaktmodel_tekenen_14.jpg
    col2: 
        - naaktmodel_tekenen_03.jpg
        - naaktmodel_tekenen_06.jpg
        - naaktmodel_tekenen_09.jpg
        - naaktmodel_tekenen_12.jpg
    col3: 
        - naaktmodel_tekenen_02.jpg
        - naaktmodel_tekenen_05.jpg
        - naaktmodel_tekenen_11.jpg
        - naaktmodel_tekenen_15.jpg
---

's Zomers organiseert de school een vierdaagse workshop naaktmodel, waarbij u de mogelijkheid krijgt om te werken met een professioneel naaktmodel. Onder begeleiding van de docent leert u de basistechnieken die nodig zijn om op een goede en professionele wijze een model te kunnen schilderen en/of tekenen.
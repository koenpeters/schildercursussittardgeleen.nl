---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Biografie Hennie Jansen
description: Biografie van docent Hennie Jansen
sharing_image: school/biografie-hennie-jansen/zelfportret-hennie-jansen.jpg
menu: 
    main:
        parent: School
        weight: 2
---

## Over schilderes en docente van schilder- en tekenschool Hennie Jansen Geleen

<div style="float:left; margin-bottom: 20px;">
    {{< img-resize hennie-bezig-aan-een-muurschildering Resize "300x" >}}margin-bottom: 10px{{< /img-resize >}}
    <div style="max-width: 300px; font-style: italic; text-align: center;">
        Docente Hennie werkt aan een muurschildering volgens een oude middeleeuwse schildertechniek. De verf die ze hier gebruikt heeft ze gemaakt van o.a. eigeel, azijn lijnolie en poederpigmenten.
    </div>
</div>

Hennie Jansen is geboren in februari 1950 in Geleen. Ze was op jonge leeftijd al bezig met tekenen, maar was pas vanaf haar 30e in de gelegenheid om een studie schilderen en tekenen aan de kunstacademie in Genk te volgen. Ze had het geluk, dat ze daar de juiste docenten trof, die haar het echte vak leerden. Na deze opleiding is ze in de leer gegaan bij een Russische lerares, om zo de oude middeleeuwse schildertechnieken onder de knie te krijgen: schilderen op gipsen panelen met o.a. eitempera, azijn, lijnolie, harsen, bladgoud en poederpigmenten.

Ze schildert al bijna 35 jaar en ze heeft een eigen stijl ontwikkeld waarin personen, bloemen, de middeleeuwen en architectuur een grote rol spelen. Ze heeft een realistische manier van schilderen, die direct opvalt en tot de verbeelding spreekt. Ze mag zich met recht een schilderes van hoog niveau noemen.

{{< img-resize hennie-schildert-abstract-werk Resize "300x" >}}float: right{{< /img-resize >}}
In 1999 kwam ze samen met haar man in de gelegenheid om een prachtig atelier te bouwen en een schilder- en tekenschool te beginnen aan de Rijksweg Noord 182 in Geleen. Sinds die tijd ontvangt ze leuke en gemotiveerde cursisten die naast veel gezelligheid, en binnen een korte tijd, een hoog niveau ontwikkelen in schilderen en tekenen.

Ook kreeg ze meer tijd om [zelf te schilderen](/galerie/eigen-werk). Ze schildert nog steeds met veel plezier.
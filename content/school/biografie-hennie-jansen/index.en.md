---
date: 2018-12-23T21:57:26+01:00
draft: false
title: "Hennie Jansen biography"
url: /school/biography-hennie-jansen
description: Biography of artist and teacher of art school Hennie Jansen Geleen
sharing_image: school/biografie-hennie-jansen/zelfportret-hennie-jansen.jpg
menu: 
    main:
        parent: School
        weight: 2
---

## About artist and teacher of art school Hennie Jansen Geleen

<div style="float:left; margin-bottom: 20px;">
    {{< img-resize hennie-bezig-aan-een-muurschildering Resize "300x" >}}margin-bottom: 10px{{< /img-resize >}}
    <div style="max-width: 300px; font-style: italic; text-align: center;">
        Hennie, the teacher, working on a mural using an old medieval paiting technique. She made the paint herself using egg yolks, vinegar linseed oil, and powder pigments.
    </div>
</div>

Hennie Jansen was born in Februari 1950 in Geleen. She's been drawing a lot from a young age, but it wasn't until she turned 30 that she had the opportunity to study painting and drawing at the art Academy in Genk, Belgium. She was very lucky being around very skilled professionals, that taught her the true profession. After successfully finishing this academy she studied old medieval techniques at the studio of a Russian teacher. There she mastered techniques like painting on plaster panels using egg tempera, vinegar, linseed oil, resin, gold leaf and powder pigments.

She has been painting for nearly 35 years and has developed her own style in which people, flowers and medieval architecture play an important role. Her paintings are realistic, immediately attract the attention and capture the imagination. She may rightfully call herself a highly skilled painter.

{{< img-resize hennie-schildert-abstract-werk Resize "300x" >}}float: right{{< /img-resize >}}
In 1999, she and her husband had the opportunity to build a professional studio and start an art school at Rijksweg Noord 182 in Geleen. Since that time she teaches painting and drawing to motivated students that, besides having a lot of fun, reach a high level in drawing and painting in a short time.

The school also gave her more time to create [her own art](/gallery/hennies-own-work/). She continues to paint and draw with great joy.
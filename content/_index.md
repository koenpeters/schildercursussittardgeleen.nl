---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Schildercursus Sittard Geleen
description: Heeft u altijd al echt goed willen leren tekenen en schilderen? Onder deskundige begeleiding, in een leuke sfeer en op hoog niveau leert Hennie Jansen u zowel de basisvaardigheden als de fijne kneepjes van het tekenen en schilderen.
keywords:
  [
    Geleen,
    Sittard,
    tekencurus,
    schildercursus,
    Hennie Jansen,
    schilderen,
    tekenen,
    schilderschool,
    tekenschool,
  ]
sharing_image: school/biografie-hennie-jansen/zelfportret-hennie-jansen.jpg
---

## Leer tekenen en schilderen op hoog niveau

Heeft u altijd al echt goed willen leren tekenen en schilderen? Onder deskundige begeleiding, in een leuke sfeer en op hoog niveau leert Hennie Jansen u zowel de basisvaardigheden als de fijne kneepjes van het tekenen en schilderen. Voor meer informatie over de school, het atelier, en de docente bekijk de rest van de site neem contact op via [e-mail](#contact) of [bel](#contact).

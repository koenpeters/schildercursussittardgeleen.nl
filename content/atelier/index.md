---
date: 2018-12-23T21:57:26+01:00
draft: false
title: Atelier
description: Het atelier van schilderschool Sittard Geleen
sharing_image: atelier/gallery/atelier-met-werkende-cursisten.jpg
menu: 
    main:
        weight: 2
menuImage: 'atelier'
gallery: 
    col1: 
        - een-naakt-schilderen.jpg
        - perspectief-in-de-vingers-krijgen.jpg
        - herfst.jpg
        - zittende-cursisten.jpg
        - cursist-in-de-serre-van-het-atelier.jpg
        - uitzicht-vanuit-atelier-op-voorkant-met-maan.jpg
        - stilleven-met-wijn.jpg
    col2:
        - tekenen-met-licht.jpg
        - kopje-tekenen.jpg
        - inspiratie-en-technieken-leren.jpg
        - atelier-met-werkende-cursisten.jpg
        - atelier-met-cursist-en-druiven.jpg
        - drie-cursisten.jpg
        - staan-achter-ezel.jpg
        - portret-in-wording.jpg
    col3:
        - de-werktafels.jpg
        - het-atelier.jpg
        - penselen.jpg
        - atelier-met-cursist.jpg
        - geconcentreerde-cursist.jpg
        - rode-rhododendron.jpg
---

## Het Atelier

Wat ooit begon als een klein atelier enkel bedoeld als plek om te werken aan haar eigen schilderijen is uitgegroeid tot een ruim atelier dat genoeg plek biedt om in groepsverband les te geven. De individuele aandacht van de docent, de vele boeken en de materialen bieden een unieke combinatie voor een prettige werksfeer. De grote ramen van het atelier bieden uitzicht op een mooie oude tuin, op de bossen en velden richting Windraak, en op Munstergeleen met zijn kerkje.

Het atelier biedt naast de noodzakelijke dingen zoals schildersezels, ruime tafels, en voldoende licht ook een leuke sfeer doordat er in groepsverband les wordt gegeven. U leert op deze manier niet alleen van de docent, maar ook van elkaar, wat het bezig zijn met kunst en het creëren van uw eigen werken nog leuker maakt dan het al is.

Binnen iedere groep werken beginners en gevorderden wat de creativiteit en de ontwikkeling sterk stimuleert.  

Heeft u interesse of vragen, neem dan contact op via het [contactformulier](#contact) of door te [bellen](#contact).

{{< youtube id="aHJK5ro6dzk" class="pinterest-album-video" >}}

{{< gallery >}}
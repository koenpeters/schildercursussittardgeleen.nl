---
date: 2018-12-23T21:57:26+01:00
draft: false
title: The studio
url: /the-studio
description: The studio of art course Sittard Geleen
sharing_image: atelier/gallery/atelier-met-werkende-cursisten.jpg
menu: 
    main:
        weight: 2
menuImage: 'atelier'
gallery: 
    col1: 
        - een-naakt-schilderen.jpg
        - perspectief-in-de-vingers-krijgen.jpg
        - herfst.jpg
        - zittende-cursisten.jpg
        - cursist-in-de-serre-van-het-atelier.jpg
        - uitzicht-vanuit-atelier-op-voorkant-met-maan.jpg
        - stilleven-met-wijn.jpg
    col2:
        - tekenen-met-licht.jpg
        - kopje-tekenen.jpg
        - inspiratie-en-technieken-leren.jpg
        - atelier-met-werkende-cursisten.jpg
        - atelier-met-cursist-en-druiven.jpg
        - drie-cursisten.jpg
        - staan-achter-ezel.jpg
        - portret-in-wording.jpg
    col3:
        - de-werktafels.jpg
        - het-atelier.jpg
        - penselen.jpg
        - atelier-met-cursist.jpg
        - geconcentreerde-cursist.jpg
        - rode-rhododendron.jpg
---

## The studio

What started as a small workshop intended only as a place to work on her own paintings, quickly grew into a real studio that offers enough room for teaching art to groups of people. The individual attention from the teacher, the many books, and the available materials offer the right combination for a pleasant working atmosphere. The large windows of the studio overlook a beautiful old garden, the woods and fields in front of the Windraak and Munstergeleen with its small church.

Besides the essentials such as easels, large tables and sufficient light the studio offers a nice atmosphere because of the group lessons. This way you not only learn from the teacher, but also from your fellow students, which makes creating art even more enjoyable. 

Each group has both beginners and more experienced students which stimulates creativity and your personal skill development. 

If you are interested or have any questions, please contact us via the [contact form](#contact) or by [calling](#contact).

{{< youtube id="aHJK5ro6dzk" class="pinterest-album-video" >}}

{{< gallery >}}
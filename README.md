# Schildercursussittardgeleen.nl

This repository contains the Hugo source code of https://schildercursussittardgeleen.nl/.

## Installation

Install [Hugo](https://gohugo.io/) and build by running

```bash
hugo
```

The result will be located in the `public` folder

For a hot reload and live preview run

```bash
hugo server
```
$.validator.setDefaults({
	errorElement: "em", // the error element type
	errorPlacement: function(error, element) {
		$(element).closest('div').find('.tooltip-wrapper').remove(); // remove all other errors before placing a new one
		error
			// put the error in the closest div (overrides the standard behaviour: beside the label)
			.appendTo(element.closest('div[class^="ym-fbox-"]'))
			// set the tooltip (you need to include the tooltip JS before this)
			.qtip();
	},
	unhighlight: function(element, errorClass, validClass) {
		// when a field is valid we remove the error class and add a valid class
		$(element).removeClass(errorClass).addClass(validClass);
		// remove the tooltip
		$(element).closest('div').find('.tooltip-wrapper').remove();
	}
});
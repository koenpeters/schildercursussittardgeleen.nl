$.fn.qtip.defaults = $.extend(true, {}, $.fn.qtip.defaults, {

	// docs: http://craigsworks.com/projects/qtip2/docs/

	content: {
		text: function(api) {
			// Retrieve content from within the tooltip selector
			return $(this).html();
		}
	}
	,position: {
		my: 'left center'
		,at: 'center right'
		,viewport: $(window)
	}
	,style: {
		classes: 'ui-tooltip-rounded'
	}
	,hide: {
		fixed: true
	}
	,show: {
		delay: 0
	}
});
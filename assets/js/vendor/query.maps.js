/*
 *
 * @requires :
 * - jQuery,
 * - maps.googleapis.com/maps/api/js
 * - infobox_packed.js (from http://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src)
 *
 */


(function($){

	var ns = 'googlemaps'
		,methods = {
			/* public methods */

			//add Markers to the map, locations is an array with json objects.
			//each json holds the data for one location: title, lat and lng are required, extra fields can be added
			addLocations : function addLocations(locations) {
				return this.each(function() {
					var o = $.data(this, ns);
					if (locations !== undefined && locations !== null) {
						$.each(locations, function(index, value) {
							_googleMapsAddLocation(o,value)
						});
					} else {
						// get the list from our predefined location
						$.getJSON(o.locationService, function(json) {
							$.each(json, function (index, value) {
								_googleMapsAddLocation(o,value)
							});
						});
					}
				})
			}

		}

	function _init(options) {

		var opts = $.extend(true, {}, $.fn.googlemaps.defaults, options);

		return this.each(function() {

			var $that = $(this)
				,o = $.extend(true, {}, opts, $that.data(opts.datasetKey));

			// add data to the defaults (e.g. $node caches etc)
			o = $.extend(true, o,
					{
					$that: $that
					,map: undefined
					,infoBox: undefined
					}
				);

			// use extend(), so no o is used by value, not by reference
			$.data(this, ns, $.extend(true, o, {}));

			/* functional plugin code */

			//Reference: https://developers.google.com/maps/documentation/javascript/
			o.map = new google.maps.Map(o.$that.get(0),o.mapOptions);

			//close active infoBox on map click
			google.maps.event.addListener(o.map, 'click', function() {_closeInfoBox(o);});


			$that.on("showDetailForLidCode", function (e,p) {
				_showInfoBox(o, o.markers[p]);
			});

		});
	}

	/* private methods, names starting with a underscore
	*/

	function _googleMapsAddLocation(o,location) {
		//Load Location
		if (isNaN(location.latitude) || isNaN(location.longitude)) {
			if (window.console) console.error("latitude/longitude in incorrect format!", location);
			return false;
		}
  		var myLatlng = new google.maps.LatLng(location.latitude,location.longitude);

		var title = (location.franchisenemer !== undefined) ? location.franchisenemer : "";
		
		var marker = new google.maps.Marker({
		    position: myLatlng,
		    icon: o.markerIcon,
			title:location.franchisenemer,
			location:location
		});

		// To add the marker to the map, call setMap();
		marker.setMap(o.map);

  		//close infoBox on map click
		google.maps.event.addListener(marker, "click", function (e) {
			_showInfoBox(o, this)
		});

		// store marker in hash (key = lidCode)
		if (location.lidCode !== undefined) {
			o.markers = o.markers || new Array();
			o.markers[location.lidCode] = marker;
		}
	}

	function _showInfoBox(o, marker) {
		_closeInfoBox(o);
		if (marker !== undefined) {
			if(o.enableInfoBox){
				//reference: http://google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/docs/reference.html
				var myOptions;
				myOptions = $.extend(true, o.infoBoxOptions,
						{
							content: _googleMapsCreateInfoContent(o,marker.location)
						}
					);

				o.infoBox = new InfoBox(myOptions);
				o.infoBox.open(o.map, marker);
			}
		}
	}

	function _closeInfoBox(o) {
		//close currently opened infoBox
		if(o.infoBox){
			//close opened infoBox
			o.infoBox.close()
			o.infoBox = undefined;
		}
	}

	function _googleMapsCreateInfoContent(o,location) {
		var content = document.createElement("div");
		content.className = 'infoBox-content';
		content.innerHTML = '' +
			'<div class="infobox-top">' +
			'	<div class="infobox-shade-rt"></div>' +
			'</div>' +
			'<div class="infobox-middle">' +
			'	<div class="infobox-shade-rm"></div>' +
			'	<div class="infobox-content">' +
			o.locationContent(location) +
			'	</div>'	+
			'</div>' +
			'<div class="infobox-bottom">' +
			'	<div class="infobox-shade-rb"></div>' +
			'</div>'
		return content;
	}

	function _log() {
		try {
			console.log.apply(this,arguments)
		} catch(err) {
			//console.log.apply doesn't work in IE
			try {
				console.log(Array.prototype.slice.call(arguments));
			} catch(err) {
			}

		}
	}

	function _error() {
		try {
			console.error.apply(this,arguments)
		} catch(err) {
			//console.error.apply doesn't work in IE
			try {
				console.error(Array.prototype.slice.call(arguments));
			} catch(err) {
			}
		}
	}

	$.fn.googlemaps = function( method ) {
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || !method ) {
			return _init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.fn.googlemaps' );
		}
	};

	/* default values for this plugin */
	$.fn.googlemaps.defaults = {
		datasetKey: 'googlemaps' //always lowercase
		,mapOptions : {
			center: new google.maps.LatLng(52.1,5),
			zoom: 7,
			minZoom:6,
			disableDefaultUI: true,
			panControl: false,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.BIG
			},
			mapTypeControl: true,
			scaleControl: false,
			streetViewControl: true,
			overviewMapControl: false,
			styles: [
				{
					/*hide labels*/
					featureType: "all",
					elementType: "labels",
					stylers: [{visibility: "on"}] // change to off to hide labels
				}
			],
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		,markerIcon: new google.maps.MarkerImage('../global/css/img/googlemaps-marker.png',
			new google.maps.Size(20, 20),
			new google.maps.Point(0,0),// The origin for this image (if sprite is used)
			new google.maps.Point(10, 10))// The anchor for this image (center of image)
		,infoBoxOptions: {
			boxClass: 'infobox'
			,disableAutoPan: false
			,maxWidth: 50
			,pixelOffset: new google.maps.Size(-22,10)
			,alignBottom:true
			,zIndex: null
			,closeBoxURL: "../global/css/img/googlemaps-close.png"
			,infoBoxClearance: new google.maps.Size(1, 1)
			,isHidden: false
			,pane: "floatPane"
			,enableEventPropagation: false
		}
		,locationContent: function(location) {
			return '' +
			'<h2>'+ location.title +'</h2>'
		}
		,enableInfoBox: true
		,locationService: '/service/vestiging/all/'
	};

})(jQuery);
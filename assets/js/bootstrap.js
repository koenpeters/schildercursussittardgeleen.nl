$(function(){
	$('li:last-child').addClass('last');
	
	$('a[href=#contact]').fancybox({
		padding: [40, 40, 40, 50]
		,margin: 8
		,arrows: false
		,minWidth: 453
		,maxWidth: 453
		,width: 453
		,minHeight: 620
		,maxHeight: 620
		,height: 620
		,afterShow: function() {
			$('#contactForm input:first').focus();
		 }
	});

	$('a[href=#route]').fancybox({
		padding: [40, 40, 40, 40]
		,margin: 8
		,arrows: false
		,minWidth: 800
		,maxWidth: 800
		,width: 800
		,minHeight: 650
		,maxHeight: 650
		,height: 650
		,afterShow: function() {
			var $mapContainer = $('#map-container');
			if ($mapContainer.children().length === 0) {
				$mapContainer.html('<iframe width="780" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.nl/maps?f=q&amp;source=s_q&amp;hl=nl&amp;geocode=&amp;q=Rijksweg+Noord+182,+Geleen&amp;aq=&amp;sll=50.980891,5.84301&amp;sspn=0.0056,0.008841&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Rijksweg+Noord+182,+6162+AP+Geleen,+Sittard-Geleen,+Limburg&amp;ll=50.980966,5.843525&amp;spn=0.021615,0.064459&amp;z=14&amp;output=embed"></iframe>');
			} 
		 }
	});
	
	$('a[href=#links]').fancybox({
		padding: [40, 40, 40, 50]
		,margin: 8
		,arrows: false
		,minWidth: 453
		,maxWidth: 453
		,width: 453
		,minHeight: 620
		,maxHeight: 620
		,height: 620
	});
	
	$('form').validate({
		submitHandler: function(form) {
			var $form = $(form);
			$form.hide();
			$('#formSending').show();
			$.post($form.attr("action"), $form.serialize()).then(function(data) {
				$('#formSending').hide();
				$('#formSuccess').show();
			});
		}
	});

	$('.pinterest-album a').fancybox({
		openEffect: 'none'
		,closeEffect: 'none'
		,nextEffect: 'none'
		,prevEffect: 'none'
		,helpers: {
			title: { type : 'inside' }
		}
	});
	
});